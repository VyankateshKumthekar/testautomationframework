package com.aspen.module.helper;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class Utilities {

	// get Screenshot
	public static String takeScreenshot(WebDriver driver, String folder) {
		Date date = new Date();
		String dateTime = date.toString().replaceAll(":", "_");
		String path = folder + "/" + dateTime + ".png";
		try {
			TakesScreenshot takeScreenshot = (TakesScreenshot) driver;
			File srcFile = takeScreenshot.getScreenshotAs(OutputType.FILE);
			File destFile = new File(path);
			FileUtils.copyFile(srcFile, destFile);
			return dateTime;

		} catch (Exception e) {
			System.err.println("Taking Screenshot failed");
		}
		return path;

	}

	// handling alert pop up
	public static void acceptJSPopup(WebDriver driver) {
		try {
			Alert alert = driver.switchTo().alert();
			Reporter.log(alert.getText(), true);
			alert.accept();
		} catch (Exception e) {
			System.err.println("Alert cannot be handled");
		}
	}

	// handling alert pop up
	public static void dismissJSPopup(WebDriver driver) {
		try {
			Alert alert = driver.switchTo().alert();
			Reporter.log(alert.getText(), true);
			alert.dismiss();
		} catch (Exception e) {
			System.err.println("Alert cannot be handled");
		}
	}

	// Scroll down to particular element location
	public static void scrollToElement(WebDriver driver, WebElement element) {
		try {
			int x = element.getLocation().getX();
			int y = element.getLocation().getY();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(" + (x - 200) + "," + (y - 200) + ")");
		} catch (Exception e) {
			System.err.println("Scrolling to element is not working ");
		}
	}

	// Scroll to top of the page
	public static void scrollTop(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(document.body.scrollHeight,0)");
		} catch (Exception e) {
			System.err.println("Scrolling top is not working ");
		}
	}

	// Scroll to down of the page
	public static void scrollDown(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("scroll(0, 600)");
			Reporter.log("Scroll down is passed", true);
		} catch (Exception e) {
			Reporter.log("Scrolling down is failed ");
		}
	}
	
	public static void scrollfooter(WebDriver driver) {
        try {
               JavascriptExecutor js = (JavascriptExecutor) driver;
               js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
               Reporter.log("Scroll down is passed", true);
        } catch (Exception e) {
               Reporter.log("Scrolling down is failed ");
        }
 }


	// context click on the specified element
	public static void actionContextClick(WebDriver driver, WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).contextClick().perform();
	}

	// left click on the element
	public static void actionClick(WebDriver driver, WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).click().build().perform();
	}

	// move to particular element
	public static void actionmove(WebDriver driver, WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

	// select class
	public static void selectByOption(String text, WebElement element) {
		Select select = new Select(element);
		List<WebElement> options = select.getOptions();
		for (WebElement option : options) {
			if (option.getText().equals(text)) {
				option.click();
			}
		}
	}

	public static void selectByVisibleText(WebElement element, String text) {
		try {
			Select select = new Select(element);
			select.selectByVisibleText(text);
			Reporter.log("Element is selected through Visible text", true);
		} catch (Exception ex) {
			Reporter.log("Element is  not able to select through visible Text", false);
		}
	}

	public static void selectByValue(WebElement element, String option) {
		try {
			Select selectElement = new Select(element);
			selectElement.selectByValue(option);
			Reporter.log("Element is selected through index", true);
		} catch (Exception ex) {
			Reporter.log("Element is  not able to select through index", false);
		}
	}
	
	public static void selectByIndex(WebElement element, int option) {
		try {
			Select selectElement = new Select(element);
			selectElement.selectByIndex(option);
			Reporter.log("Element is selected through index", true);
		} catch (Exception ex) {
			Reporter.log("Element is  not able to select through index", false);
		}
	}
	
	

	// get element color
	public static void getElementColor(WebElement element) {
		try {
			String color = element.getCssValue("color");
			Reporter.log("The color of the element " + element.getText() + " is " + color, true);
			String hex = Color.fromString(color).asHex();
			Reporter.log("The hex value of the element's color code is " + hex, true);
		} catch (Exception e) {
			System.err.println("Element color cannot be found");
		}
	}

	// get background color
	public static void getElementBackgroundColor(WebElement element) {
		try {
			String color = element.getCssValue("background-color");
			Reporter.log("The background color of the element " + element.getText() + " is " + color, true);
			String hex = Color.fromString(color).asHex();
			Reporter.log("The hex value of the element's color code is " + hex, true);
		} catch (Exception e) {
			System.err.println("Element background 1color cannot be found");
		}
	}

/*	public static boolean isElementPresent(String actionXpath, WebDriver driver) {
		try {
			List<WebElement> ElementCheck = driver.findElements(By.xpath(actionXpath));
			if (ElementCheck.size() != 0) {
				Reporter.log("Element is  Present", true);
			} else {
				Reporter.log("Element is not Present", true);
			}
			return true;
		} catch (Exception e) {
			Reporter.log("Element visibility is failed", false);
		}
		return true;

	}*/
	
    public static boolean isElementPresent(String actionXpath, WebDriver driver) {
        boolean returnVal = false;
        try {
              List<WebElement> ElementCheck = driver.findElements(By.xpath(actionXpath));
              if (ElementCheck.size() != 0) {
                    Reporter.log("Element is  Present", true);
                    returnVal = true;
              } else {
                    Reporter.log("Element is not Present", true);
                    returnVal = false;
              }
              
        } catch (Exception e) {
              Reporter.log("Element visibility is failed", false);
        }           
        return returnVal;
  }


	public static void waitTillElementIsClickable(WebDriver driver, WebDriverWait wait, WebElement element) {
		try {
			wait = new WebDriverWait(driver, 80);
			WebElement clickElement = wait.until(ExpectedConditions.elementToBeClickable(element));
			clickElement.click();
			// ExtentListeners.testReport.get().info("Clicking on : " + element);
		} catch (Exception ex) {
			Reporter.log("Element is not clickable", false);
		}
	}

	public static void wait(int timeInSeconds) {
		try {
			Thread.sleep(timeInSeconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Properties loadProperty(String path) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(path));

		} catch (Exception e) {
			System.err.println("Properties file cannot be handled");
		}
		return properties;
	}

	public static void waitForElement(WebDriver driver, int milliSeconds) {
		synchronized (driver) {
			try {
				driver.wait(milliSeconds * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static boolean isElementPresentForAllLocators(WebDriver driver, By by) {

		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public static void selectPdropdown(List<WebElement> list, String value) {
		for (WebElement one : list) {
			String text = one.getText();
			if (text.equals(value)) {
				one.click();
			}

		}
	}
	public static void scrollToElementDeleteOrganization(WebDriver driver, WebElement element) {
		try {
			int x = element.getLocation().getX();
			int y = element.getLocation().getY();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(" + (x - 600) + "," + (y - 600) + ")");
		} catch (Exception e) {
			System.err.println("Scrolling to element is not working ");
		}
	}
	 public static String randomNumber()
    {
           String generatednumber=RandomStringUtils.randomNumeric(4);//10 is length
           return generatednumber;
    }
	public static String randomInteger() {
        Random random = new Random();
        Integer id=random.nextInt(999);
        return id.toString();
        
  }
	public static String randomString() {
        String generatedString=RandomStringUtils.randomAlphabetic(5);
        return generatedString;
        
  }
	public static void waitForLoading(WebDriver driver,WebDriverWait wait) {
		WebElement loading = driver.findElement(By.xpath("//div[@class=\"ngx-loading-text center-center\"]"));
		wait.until(ExpectedConditions.invisibilityOf(loading));
	}

  	
  	public static String getYesterdayDate() {
  		int dayInMilliSeconds = 1000 * 60 * 60 * 24;
  	    Date date = new Date();
  	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
  	    String prevDate = dateFormat.format(date.getTime() - dayInMilliSeconds);
  		return prevDate;		
  	}
  	
  	//Removing leading zero from the date if available
  	public static String replaceLeadingZero (String dateToSelect) {		
  		String str = dateToSelect;
  		String removedLeadingZero = "^0+";  
  		String afterRemovedZero = str.replaceAll(removedLeadingZero, "");
  		Reporter.log( "String: " + afterRemovedZero , true);
  		return afterRemovedZero;
  	}
  	
	public static String randomInteger(int digit) {
		String integer="";
		for(int i=1; i<=digit;i++ ) {
			integer+=9;
		}
		Random random = new Random();
		Integer value=random.nextInt(Integer.parseInt(integer));
		return value.toString();
    }
	
	public static String getTodayDate() {
  		Date date = new Date();  
  		SimpleDateFormat formatter = new SimpleDateFormat("dd");  
  		String desiredDateToSelect= formatter.format(date); 
          return desiredDateToSelect;
  	}
  	
  	public static String getTodayDateForSmartLetter() {
  		Date date = new Date();  
  		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  
  		String desiredDateToSelect= formatter.format(date); 
          return desiredDateToSelect;
  	}
  	

  	public static String getYesterdayDate1() {
  		int dayInMilliSeconds = 1000 * 60 * 60 * 24;
  	    Date date = new Date();
  	    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
  	    String prevDate = dateFormat.format(date.getTime() - dayInMilliSeconds);
  		return prevDate;		
  	}
  	
  	public static String randomNumber(int digit) {
        String generatednumber = RandomStringUtils.randomNumeric(digit);// 10 is length
        return generatednumber;
    }
  	
public static void takeScreenshotpdf(WebDriver driver, String property , String folder) {
		
		String path = folder + "/" + property + ".pdf";
		try {
			TakesScreenshot takeScreenshot = (TakesScreenshot) driver;
			File srcFile = takeScreenshot.getScreenshotAs(OutputType.FILE);
			File destFile = new File(path);
			FileUtils.copyFile(srcFile, destFile);
		

		} catch (Exception e) {
			System.err.println("Taking Screenshot failed");
		}
	

	}
  	
	public static void selectonebyone(List<WebElement> list, String value) {
		for (WebElement one : list) {
			
			Select s= new Select(one);
			s.selectByVisibleText(value);
		}
		}

}
