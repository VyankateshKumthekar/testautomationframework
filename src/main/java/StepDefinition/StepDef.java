package StepDefinition;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDef {
	

	public static WebDriver driver = null;
	@Given("^User is on EPMS3 Application Log-in Page$")
	public void user_is_on_EPMS_Application_Log_in_Page() {
	    
	    System.setProperty("webdriver.chrome.driver","C:\\chromedriver_win32\\chromedriver.exe");
		 driver = new ChromeDriver();
		 driver.get("https://tdqa1epm3web.aws-admi.com/EPMSP3/#/login");
		 //driver.get("https://tdqa1epm3web.aws-admi.com/EPMSP3/#/login");
		 driver.manage().window().maximize();
		 
	}

	@When("^User enters \"([^\"]*)\"  and \"([^\"]*)\"$")
	public void user_enters_and(String arg1, String arg2) throws InterruptedException {
		
		
		Thread.sleep(2000);
	    driver.findElement(By.cssSelector("body > app-root > div > app-sidebar > ng-sidebar-container > div > div > main-content > div > app-login > form > div > div:nth-child(2) > div.summary_box_content.clearfix > div:nth-child(1) > input")).sendKeys(arg1);
	    Thread.sleep(2000);  
	    driver.findElement(By.cssSelector("body > app-root > div > app-sidebar > ng-sidebar-container > div > div > main-content > div > app-login > form > div > div:nth-child(2) > div.summary_box_content.clearfix > div:nth-child(2) > input")).sendKeys(arg2);
	    Thread.sleep(2000);
	    
	}

	@Then("^User clicks on log-in button$")
	public void user_clicks_on_log_in_button() {
	    
		
		driver.findElement(By.cssSelector("body > app-root > div > app-sidebar > ng-sidebar-container > div > div > main-content > div > app-login > form > div > div:nth-child(2) > div.summary_box_content.clearfix > div.form-group > button")).click();
	    
	}

	@Then("^User is on WelCome Page$")
	public void user_is_on_HambergerMenu_Home_Page()  {
		String title = driver.getTitle();
		 System.out.println("Login Test Case-->>Hamberger Menu WelCome Page- "+ title);
		 Assert.assertEquals("login", title);
		 
	}
	
		 @When("^User clicks on HambergerMenu Bar$")
		 public void user_clicks_on_HambergerMenu_Bar()  {
			 
			 //driver.findElement(By.id("faBar")).click();
			 
			 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
			 //driver.findElement(By.cssSelector("body > app-root > div > app-sidebar > ng-sidebar-container > div > div > main-content > div > app-organization-management > app-org-management > app-card > div > div.card-body.ng-star-inserted > card-body > app-card > div > div.card-header.bg-primary.text-white.ng-star-inserted > card-header > button:nth-child(2)")).click();
		     driver.findElement(By.xpath("/html/body/app-root/div/app-sidebar/ng-sidebar-container/div/div/main-content/div/app-login/form/div/div[2]/div[2]/div[5]/button")).click();
	         driver.findElement(By.xpath("/html/body/app-root/div/app-sidebar/ng-sidebar-container/div/div/main-content/div/app-organization-management/app-org-management/app-card/div/div[2]/card-body/app-card/div/div[1]/card-header/button[1]")).click();	
		 }
		 

		 @Then("^User clicks on Accouting Tab$")
		 public void user_clicks_on_Accouting_Tab()  {
		     // Write code here that turns the phrase above into concrete actions
		     
		 }

		 @Then("^User clicks on Maintenance Tab$")
		 public void user_clicks_on_Maintenance_Tab()  {
		     // Write code here that turns the phrase above into concrete actions
		     
		 }

		 @Then("^User is on PAyment Adjustment Tab Screen$")
		 public void user_is_on_PAyment_Adjustment_Tab_Screen()  {
		     // Write code here that turns the phrase above into concrete actions
		     
		 }
		 
	    
	}

