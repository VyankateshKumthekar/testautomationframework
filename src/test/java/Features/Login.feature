Feature: EPMS3 Application Log-in to View Hamberger Menu

Scenario Outline: EPMS3 Application Log-in to View Hamberger Menu for Billing Transaction FRS 

    Given User is on EPMS3 Application Log-in Page 
    When User enters "<arg1>"  and "<arg2>"
    Then User clicks on log-in button
    Then User is on WelCome Page
    
Examples: 
        |arg1| arg2 |
        |facilityFinAdmin| !@#123QWEqwe |


Scenario: Validate the Billing Transaction Home Page to add a new Adjustment Code
    When User clicks on HambergerMenu Bar
    Then User clicks on Accouting Tab 
    Then User clicks on Maintenance Tab
    Then User is on PAyment Adjustment Tab Screen